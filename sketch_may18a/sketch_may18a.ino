#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <time.h>
#include <BearSSLHelpers.h>
#include "ca_cert.h"
#include "client_cert.h"
#include "private_key.h"

// WiFi settings
const char* ssid = "INTERNET LEONOR";
const char* password = "7970812423";

// AWS IoT settings
const char* aws_endpoint = "a32w8s71hfr88r-ats.iot.us-west-1.amazonaws.com";
const int port = 8883;

// NTP settings
const char* ntpServer = "pool.ntp.org";
const long gmtOffset_sec = 0;
const int daylightOffset_sec = 0;

// Flag to track connection status
bool awsConnected = false;

// Create a secure client
BearSSL::WiFiClientSecure net;
PubSubClient client(net);


void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}


void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
}


void setup() {
  Serial.begin(115200);
  WiFi.begin(ssid, password);

  // Wait for WiFi connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("WiFi connected");

  // Initialize NTP
  configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);

  // Wait for time to be set
  time_t now = time(nullptr);
  while (now < 8 * 3600 * 2) {
    delay(500);
    Serial.print(".");
    now = time(nullptr);
  }
  Serial.println("");
  Serial.println("Time synchronized");

  // Load certificate and private key
  BearSSL::X509List trustAnchor(ca_cert);
  BearSSL::X509List clientCert(client_cert);
  BearSSL::PrivateKey clientKey(private_key);

  net.setTrustAnchors(&trustAnchor);
  net.setClientRSACert(&clientCert, &clientKey);

  client.setServer(aws_endpoint, port);
  client.setKeepAlive(60);  // Increase keepalive interval to 60 seconds

  // Check and connect to AWS IoT
  if (client.connect("ESP8266Client")) {
    if (!awsConnected) {
      Serial.println("Connected to AWS IoT");
      awsConnected = true; // Set the flag to true
    }
  } else {
    Serial.print("Connection to AWS IoT failed, rc=");
    Serial.print(client.state());
    Serial.println(" try again in 5 seconds");

    // Print detailed SSL error information
    int ssl_error = net.getLastSSLError();
    Serial.print("Last SSL error code: ");
    Serial.println(ssl_error);
  }
}

void loop() {
  if (!client.connected()) {
    // Reconnect if connection is lost
    if (client.connect("ESP8266Client")) {
      if (!awsConnected) {
        Serial.println("Connected to AWS IoT");
        awsConnected = true; // Set the flag to true
      }
    }
  }
  
  client.loop(); // Handle incoming messages and keep the connection alive
}