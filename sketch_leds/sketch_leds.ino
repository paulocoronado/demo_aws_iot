const int ledPin1 = 16;  // LED connected to D0
const int ledPin2 = 4;  // LED connected to D2
const int ledPin3 = 2;  // LED connected to D4

void setup() {
  // Set the LED pins as OUTPUT
  pinMode(ledPin1, OUTPUT);
  pinMode(ledPin2, OUTPUT);
  pinMode(ledPin3, OUTPUT);
}

void loop() {
  // Turn on LED connected to D0
  digitalWrite(ledPin1, HIGH);
  delay(500); // Wait for 500 milliseconds
  digitalWrite(ledPin1, LOW);
  
  // Turn on LED connected to D1
  digitalWrite(ledPin2, HIGH);
  delay(500); // Wait for 500 milliseconds
  digitalWrite(ledPin2, LOW);
  
  // Turn on LED connected to D2
  digitalWrite(ledPin3, HIGH);
  delay(500); // Wait for 500 milliseconds
  digitalWrite(ledPin3, LOW);
}
