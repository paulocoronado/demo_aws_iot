const int trigPin = 14;  // TRIG connected to D5
const int echoPin = 13;  // ECHO connected to D7

void setup() {
  // Initialize the serial communication
  Serial.begin(9600);

  // Set the trigPin as an OUTPUT
  pinMode(trigPin, OUTPUT);
  
  // Set the echoPin as an INPUT
  pinMode(echoPin, INPUT);
}

void loop() {
  // Clear the trigPin by setting it LOW
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);

  // Trigger the sensor by setting the trigPin HIGH for 10 microseconds
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  // Read the time for the echoPin to go HIGH
  long duration = pulseIn(echoPin, HIGH);

  // Calculate the distance
  // Speed of sound wave divided by 2 (there and back)
  int distance = duration * 0.034 / 2;

  // Print the distance on the serial monitor
  Serial.print("Distance: ");
  Serial.print(distance);
  Serial.println(" cm");

  // Add a delay before the next measurement
  delay(500);
}